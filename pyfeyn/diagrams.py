"""Classes for the actual diagram containers."""

import pyx
from pyfeyn import config, pyxversion
from distutils.version import StrictVersion as Version
from os.path import splitext

## Diagram class
class FeynDiagram:
    """The main PyFeyn diagram class."""    
    currentDiagram = None

    def __init__(self, objects=None, canvas=None):
        """Objects for holding a set of Feynman diagram components."""
        self.__objs = objects
        if self.__objs is None:
            self.__objs = []
        self.highestautolayer = 0
        if canvas is None:
           self.currentCanvas = pyx.canvas.canvas()
        else:
           self.currentCanvas = canvas
        FeynDiagram.currentDiagram = self


    def add(self, *objs):
        """Add an object to the diagram."""
        for obj in objs:
            if config.getOptions().DEBUG:
                print("#objs = %d" % len(self.__objs))
            offset = 0
            if "layeroffset" in obj.__dict__:
                #print "offset =", obj.layeroffset
                offset = obj.layeroffset
            self.highestautolayer += 1
            obj.setDepth(self.highestautolayer + offset)
            if config.getOptions().DEBUG:
                print("Object %s layer = %d + %d = %d" % \
                      (obj.__class__, self.highestautolayer, offset,
                       self.highestautolayer + offset))
            self.__objs.append(obj)


    def drawToCanvas(self):
        """Draw the components of this diagram in a well-defined order."""
        if config.getOptions().DEBUG:
            print("Final #objs = %d" % len(self.__objs))
        if config.getOptions().VDEBUG:
            print("Running in visual debug mode")

        ## Sort drawing objects by layer
        drawingobjs = self.__objs
        try:
            drawingobjs.sort()
        except:
            if config.getOptions().DEBUG:
                print("Sorting between visible objects failed")
            pass
            
        ## Draw each object
        for obj in drawingobjs:
            if config.getOptions().DEBUG:
                print("Depth = ", obj.getDepth())
            obj.draw(self.currentCanvas)

        return self.currentCanvas


    def draw(self, outfile, enlargement=1, resolution=None, device=None):
        """Draw the diagram to a file, with the filetype (EPS, JPG, PDF, or PNG)
        derived from the file extension."""
        c = self.drawToCanvas()
        if c is None:
            print("Warning: Nothing to draw")
            return
        froot, fext = splitext(outfile)
        if config.getOptions().DEBUG:
            print("File extension for export: %s" % fext)

        if fext in ['.eps', '.pdf', '.svg']:
            if resolution is not None:
                print("Warning: resolution parameter ignored for vector "
                      "format %s" % fext[1:])
            if device is not None:
                print("Warning: device parameter ignored for vector "
                      "format %s" % fext[1:])
            c.writetofile(outfile, page_bboxenlarge=pyx.unit.t_pt*enlargement)

        elif fext in ['.jpg', '.png']:
            c.writeGSfile(outfile, page_bboxenlarge=pyx.unit.t_pt*enlargement,
                          resolution=100 if resolution is None else resolution,
                          device=device)

        else:
            print("Output file format not accepted; must be one of "
                  "eps, jpg, pdf, png, or svg.")
