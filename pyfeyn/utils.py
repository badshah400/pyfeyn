"""Utility functions and classes for PyFeyn"""

import pyx
from pyfeyn.diagrams import FeynDiagram
from pyfeyn import config


## Default units
defunit = pyx.unit.cm
todefunit = pyx.unit.tocm


def sign(x):
    """Get the sign of a numeric type"""
    if x < 0:
        return -1
    if x > 0:
        return 1
    if x == 0:
        return 0


class Visible:
    def isVisible(self):
        """Check if this instance is visible."""
        return True

    def getPath(self):
        """Return the path of this instance."""
        return None

    def getVisiblePath(self):
        """Return the visible path of this instance."""
        return self.getPath()

    def setDepth(self, depth):
        """Set the depth at which this instance lives."""
        self.depth = depth
        return self

    def getDepth(self):
        """Return the depth at which this instance lives."""
        if "depth" in self.__dict__:
            return self.depth
        else:
            return None
        
    def cmp_debugmsg(self, other):
        print("Comparing visible classes: ", \
              self.__class__, "->", self.getDepth(), "vs.", \
              other.__class__, "->", other.getDepth())

    def __eq__(self, other):
        if other is None:
            return -1

        if config.getOptions().DEBUG:
            self.cmp_debugmsg(other)
        else:
            return (self.getDepth() == other.getDepth())

    def __ne__(self, other):
        if other is None:
            return -1

        return not(__eq__(self, other))

    def __lt__(self, other):
        if other is None:
            return -1

        if config.getOptions().DEBUG:
            self.cmp_debugmsg(other)
        else:
            return (self.getDepth() < other.getDepth())

    def __le__(self, other):
        if other is None:
            return -1

        if config.getOptions().DEBUG:
            self.cmp_debugmsg(other)
        else:
            return (self.getDepth() <= other.getDepth())

    def __gt__(self, other):
        if other is None:
            return -1

        if config.getOptions().DEBUG:
            self.cmp_debugmsg(other)
        else:
            return (self.getDepth() > other.getDepth())

    def __ge__(self, other):
        if other is None:
            return -1

        if config.getOptions().DEBUG:
            self.cmp_debugmsg(other)
        else:
            return (self.getDepth() >= other.getDepth())

