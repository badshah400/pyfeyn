#! /usr/bin/env python

from setuptools import setup, find_packages
from pyfeyn import __version__ as pyfeyn_version

longdesc = """PyFeyn is a package which makes drawing Feynman diagrams simple and programmatic.
Feynman diagrams are important constructs in perturbative field theory, so being able to draw them
in a programmatic fashion is important if attempting to enumerate a large number of diagram
configurations is important. The output quality of PyFeyn diagrams (into PDF or EPS formats)
is very high, and special effects can be obtained by using constructs from PyX, which PyFeyn
is based around."""

## Setup definition
setup(name = 'pyfeyn',
      version = pyfeyn_version,
      packages=find_packages(),
      include_package_data=True,
      install_requires = ['PyX >= 0.15'],
      tests_require = ['pytest', 'cv2', 'skimage'],
      scripts = ['mkfeyndiag'],
      author = 'Andy Buckley, Georg von Hippel',
      author_email = 'pyfeyn@projects.hepforge.org',
      url = 'https://pyfeyn.hepforge.org/',
      description = 'An easy-to-use Python library to help physicists draw Feynman diagrams.',
      long_description = longdesc,
      keywords = 'feynman hep physics particle diagram',
      license = 'GPL-2.0-or-later',
      classifiers = ['Development Status :: 4 - Beta',
                     'Environment :: Console',
                     'Intended Audience :: Science/Research',
                     'License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)',
                     'Operating System :: OS Independent',
                     'Programming Language :: Python',
                     'Topic :: Artistic Software',
                     'Topic :: Scientific/Engineering :: Physics']
      )
