#!/usr/bin/python3
# vim: set ai et ts=4 sw=4 tw=80:

from skimage.metrics import structural_similarity as ssim
import cv2
from pyfeyn.user import *

def test_arrow_label():
    fd = FeynDiagram()
    
    in1 = Point(-4,  2)
    in2 = Point(-4, -2)
    out = Point(0, 0)
    in_vtx = Vertex(-2, 0, mark=CIRCLE)

    fa1 = Fermion(in1, in_vtx).addArrow().addLabel(r"\Pelectron")
    fa2 = Fermion(in_vtx, in2).addArrow().addLabel(r"\Ppositron")
    fa2.addParallelArrow(size=0.1, displace=-0.06, sense=-1)
    bos = Photon(in_vtx, out).addLabel(r"\PZ", displace=-0.5)
    bos.addParallelArrow(displace=-0.2)

    fd.draw("test/pyfeyn-arrow-label.png")
    pfimg = cv2.imread('test/pyfeyn-arrow-label.png')
    orig  = cv2.imread('test/test_images/arrow-label.png')
    s     = ssim(pfimg, orig, multichannel=True)
    print(s)
    assert s == 1.0

