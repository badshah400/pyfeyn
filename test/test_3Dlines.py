#!/usr/bin/python3
# vim: set ai et ts=4 sw=4 tw=80:

from skimage.metrics import structural_similarity as ssim
import cv2
from pyfeyn.user import *

def test_3DGluon():
    fd = FeynDiagram()
    
    v1 = Vertex(-2, 0, mark=CircleMark())
    v2 = Vertex(+2, 0, mark=CircleMark())
    
    g2 = Gluon(v1, v2).bend(1.5).set3D()
    
    fd.draw("test/pyfeyn-3DGluon.png")
    pfimg = cv2.imread('test/pyfeyn-3DGluon.png')
    orig  = cv2.imread('test/test_images/3DGluon.png')
    s     = ssim(pfimg, orig, multichannel=True)
    print(s)
    assert s == 1.0

def test_3DGluino():
    fd = FeynDiagram()
    
    v1 = Vertex(-2, 0, mark=CircleMark())
    v2 = Vertex(+2, 0, mark=CircleMark())
    
    g1 = Gluino(v1, v2).set3D()
    
    fd.draw("test/pyfeyn-3DGluino.png")
    pfimg = cv2.imread('test/pyfeyn-3DGluino.png')
    orig  = cv2.imread('test/test_images/3DGluino.png')
    s     = ssim(pfimg, orig, multichannel=True)
    print(s)
    assert s == 1.0

def test_3DGaugino():
    fd = FeynDiagram()
    
    v1 = Vertex(-2, 0, mark=CircleMark())
    v2 = Vertex(+2, 0, mark=CircleMark())
    
    g2 = Gaugino(v1, v2).bend(-1.5).set3D()
    
    fd.draw("test/pyfeyn-3DGaugino.png")
    pfimg = cv2.imread('test/pyfeyn-3DGaugino.png')
    orig  = cv2.imread('test/test_images/3DGaugino.png')
    s     = ssim(pfimg, orig, multichannel=True)
    print(s)
    assert s == 1.0

def test_3DGraviton():
    fd = FeynDiagram()
    
    v1 = Vertex(-2, 0, mark=CircleMark())
    v2 = Vertex(+2, 0, mark=CircleMark())
    
    g2 = Graviton(v1, v2).set3D()
    
    fd.draw("test/pyfeyn-3DGraviton.png")
    pfimg = cv2.imread('test/pyfeyn-3DGraviton.png')
    orig  = cv2.imread('test/test_images/3DGraviton.png')
    s     = ssim(pfimg, orig, multichannel=True)
    print(s)
    assert s == 1.0

