#!/usr/bin/python3
# vim: set ai et ts=4 sw=4 tw=80:

from skimage.metrics import structural_similarity as ssim
import cv2
from pyfeyn.user import *

def test_blobs():
    fd = FeynDiagram()
    p1 = Point(2, -2)
    p2 = Point(-2, 2)
    p4 = p1.midpoint(p2)
    
    c1 = Circle(center=p1, radius=0.5, stroke=[THICK2], fill=[RED], points=[p1])
    c2 = Circle(center=p2, radius=0.3, stroke=[THICK2], fill=[CROSSHATCHED45], points=[p2])
    e1 = Ellipse(center=p4, xradius=0.5, yradius=1.0,
                 fill=[HATCHED0], points=[p4])
    
    l1 = Fermion(c1, c2).addStyle(THICK3).addStyle(BLUE)
    l2 = Photon(midpoint(p1,p4), midpoint(p4, p2)).bend(1.5).setStyles([GREEN, THICK3])
    fd.draw('test/pyfeyn-blobs.png')
    pfimg = cv2.imread('test/pyfeyn-blobs.png')
    orig  = cv2.imread('test/test_images/blobs.png')
    s     = ssim(pfimg, orig, multichannel=True)
    print(s)
    assert s == 1.0

