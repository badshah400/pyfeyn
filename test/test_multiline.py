#!/usr/bin/python3
# vim: set ai et ts=4 sw=4 tw=80:

from skimage.metrics import structural_similarity as ssim
import cv2
from pyfeyn.user import *

def test_multiline():
    fd = FeynDiagram()
    p1 = Point(-2, 0)
    p2 = Point(2,0)
    fa1 = MultiLine(p1, p2).bend(0.5).addArrow()
    c1 = Circle(center=p1, radius=0.5, fill=[VIOLET], points=[p1])
    c2 = Circle(center=p2, radius=0.5, fill=[BURNTORANGE], points=[p2])
 
    fd.draw('test/pyfeyn-multiline.png')
    pfimg = cv2.imread('test/pyfeyn-multiline.png')
    orig  = cv2.imread('test/test_images/multiline.png')
    s     = ssim(pfimg, orig, multichannel=True)
    print(s)
    assert s == 1.0

