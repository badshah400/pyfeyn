# PyFeyn #

<https://pyfeyn.hepforge.org/>

**PyFeyn** is a Python-language based system for drawing Feynman diagrams. It was inspired by the C++ FeynDiagram system, and aims to provide the same functionality and quality of output as that, with the added benefits of a modern interpreted language, an improved interface and output direct to both EPS and PDF. Behind the scenes, PyFeyn uses the excellent PyX system - you can use PyX constructs in PyFeyn diagrams if you want, too.

## Examples ##

There are a range of demonstrations of PyFeyn's capabilities on the [examples page](https://pyfeyn.hepforge.org/examples).

## Getting PyFeyn ##

Please see the [installation guide page](https://pyfeyn.hepforge.org/install) for installation instructions if you're unfamiliar with installing Python packages. Note that PyFeyn depends on [PyX](http://pyx.sf.net/) as its rendering back-end, so you'll need to have `PyX >= 0.15` installed before PyFeyn will work.

## Using PyFeyn ##

PyFeyn is used primarily as a Python package, i.e. a class library of useful object types and operations for drawing diagrams. Thanks to Georg von Hippel's work on our FeynML interface, we also have a command line program called `mkfeyndiag` which will build diagrams direct from FeynML XML files without having to write any Python at all.

There's currently no "proper" manual, but there is automatically generated [code documentation](https://pyfeyn.hepforge.org/doc/) which should hopefully be enough to work as a library reference. A more amenable introduction may be to have a look at the [gallery examples](https://pyfeyn.hepforge.org/examples) to get a feel for how it works - hopefully these are fairly self-explanatory, please let us know by email (to pyfeyn-AT-projects.hepforge.org) if you get confused.

